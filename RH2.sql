--1. Crear un tablespace con el nombre EMPLEADOS_DAT de 100 MB, 
--   dicho tablespace debe estar conformado por 2 datafiles de 50
--   Megas cada uno. 3 puntos.
CREATE TABLESPACE EMPLEADOS_DAT DATAFILE 'EMPLEADOS_DAT1.dbf' SIZE 50m;
ALTER TABLESPACE EMPLEADOS_DAT ADD DATAFILE 'EMPLEADOS_DAT2.dbf' SIZE 50m;
--******************************************************************************
--2. Crear el esquema con el nombre RHUMANO, 
--   debe asignarle el tablespace EMPLEADOS_DAT y asignarle 
--   los roles de conexión y resource 3 puntos.
CREATE USER "RHUMANO" 
DEFAULT TABLESPACE "EMPLEADOS_DAT" 
TEMPORARY TABLESPACE "TEMP" 
IDENTIFIED BY "ADMIN12345" 
ACCOUNT UNLOCK;

ALTER USER "RHUMANO" QUOTA UNLIMITED ON "EMPLEADOS_DAT"; 

GRANT "CONNECT" TO RHUMANO; 
GRANT "RESOURCE" TO RHUMANO; 
 
GRANT CREATE TABLE TO RHUMANO;
GRANT CREATE VIEW TO RHUMANO;
--******************************************************************************
--3. Una vez creado el esquema debe crear las tablas como se indica 
--   a continuación con sus respectivas (PK, FK, CK, UK) 6 puntos.
CREATE TABLE PUESTO
(CODIGO INT PRIMARY KEY,
NOMBRE VARCHAR(100),
SALARIO FLOAT
);
CREATE TABLE DEPARTAMENTO
(CODIGO INT PRIMARY KEY,
NOMBRE VARCHAR(100),
PRESUPUESTO FLOAT,
GASTOS FLOAT
);
CREATE TABLE EMPLEADO
(CODIGO INT PRIMARY KEY,
IDENTIFICACION VARCHAR(100) UNIQUE,
NOMBRE VARCHAR(100),
APELLIDO1 VARCHAR(100),
APELLIDO2 VARCHAR(100),
CODIGO_DEPARTAMENTO INT,
CODIGO_PUESTO INT,
ESTADO CHAR,
CONSTRAINT FK_EMPLEADO_DEPARTAMENTO_E FOREIGN KEY (CODIGO_DEPARTAMENTO) REFERENCES DEPARTAMENTO(CODIGO),
CONSTRAINT FK_EMPLEADO_PUESTO_E FOREIGN KEY (CODIGO_PUESTO) REFERENCES PUESTO(CODIGO),
CONSTRAINT CK_ESTADO_E CHECK (ESTADO IN ('a', 'i'))
);
--******************************************************************************
--4. Cargas los datos como se indica 0 puntos
INSERT INTO DEPARTAMENTO VALUES(1, 'desarrollo', 120000, 6000);
INSERT INTO DEPARTAMENTO VALUES(2, 'sistemas', 150000, 21000);
INSERT INTO DEPARTAMENTO VALUES(3, 'recursos humanos', 280000, 25000);
INSERT INTO DEPARTAMENTO VALUES(4, 'contabilidad', 110000, 3000);
INSERT INTO DEPARTAMENTO VALUES(5, 'ventas', 375000, 380000);
INSERT INTO DEPARTAMENTO VALUES(6, 'proyectos', 0, 0);
INSERT INTO DEPARTAMENTO VALUES(7, 'publicidad', 0, 1000);
INSERT INTO PUESTO VALUES(1, 'programador', 3500 );
INSERT INTO PUESTO VALUES(2, 'director técnico / director', 5000 );
INSERT INTO PUESTO VALUES(3, 'analista de sistemas', 3200 );
INSERT INTO PUESTO VALUES(4, 'arquitecto de bases de datos', 4500 );
INSERT INTO PUESTO VALUES(5, 'consultor de ciberseguridad', 5500 );
INSERT INTO PUESTO VALUES(6, ' ingeniero en integración de datos', 6500);
INSERT INTO PUESTO VALUES(7, 'jefe de departamento', 7000);
INSERT INTO EMPLEADO VALUES(1, '32481596f', 'aarón', 'rivero', 'gómez', 1,1,'a');
INSERT INTO EMPLEADO VALUES(6, '38382980m', 'maría', 'santana', 'moreno', 1,2,'a');
INSERT INTO EMPLEADO VALUES(11, '67389283a', 'marta','herrera', 'gil', 1,3,'a');
INSERT INTO EMPLEADO VALUES(2, 'y5575632d', 'adela', 'salas', 'díaz', 2,2,'a');
INSERT INTO EMPLEADO VALUES(7, '80576669x', 'pilar', 'ruiz', NULL, 2,3,'a');
INSERT INTO EMPLEADO VALUES(9, '56399183d', 'juan', 'gómez', 'lópez', 2,5,'a');
INSERT INTO EMPLEADO VALUES(3, 'r6970642b', 'adolfo', 'rubio', 'flores', 3,7,'a');
INSERT INTO EMPLEADO VALUES(8, '71651431z', 'pepe', 'ruiz', 'santana', 3,NULL,'a');
INSERT INTO EMPLEADO VALUES(4, '77705545e', 'adrián', 'suárez', NULL, 4,NULL,'a');
INSERT INTO EMPLEADO VALUES(5, '17087203c', 'marcos', 'loyola', 'méndez', 5,NULL,'a');
INSERT INTO EMPLEADO VALUES(10, '46384486h', 'diego','flores', 'salas', 5,NULL,'a');
INSERT INTO EMPLEADO VALUES(12, '41234836r', 'irene','salas', 'flores', NULL,NULL,'a');
INSERT INTO EMPLEADO VALUES(13, '82635162b', 'juan antonio','sáez', 'guerrero', NULL,NULL,'a');
--******************************************************************************
--5. Crear una vista con el nombre V_EMPLEADOS con las columnas,Código Empleado,
--   Nombre Completo, Nombre del puesto, Nombre del departamento, Salario, Tome 
--   en cuenta que en la consulta existen valores NULL, los cuales debe 
--   visualizarse como 'NO INDICA'. 6 puntos.
CREATE VIEW V_EMPLEADOS AS
SELECT E.CODIGO, (E.NOMBRE||' '||E.APELLIDO1||' '||E.APELLIDO2) AS NOMBRE,
NVL(P.NOMBRE,'NO INDICA') AS PUESTO,NVL(D.NOMBRE,'NO INDICA') AS DEPARTAMENTO FROM EMPLEADO E
LEFT JOIN PUESTO P
ON P.CODIGO=E.CODIGO_PUESTO
LEFT JOIN DEPARTAMENTO D
ON D.CODIGO=E.CODIGO_DEPARTAMENTO;
--******************************************************************************
--6. Crear la tabla. 1 puntos
CREATE TABLE HISTORICO_PUESTO
(ID_HISTORICO INT PRIMARY KEY,
CODIGO_EMPLEADO INT,
PUESTO_ANTERIOR INT,
PUESTO_ACTUAL INT,
FECHA_CAMBIOS DATE,
CONSTRAINT FK_CODIGO_EMPLEADO_HP FOREIGN KEY(CODIGO_EMPLEADO) REFERENCES EMPLEADO(CODIGO)
);
--******************************************************************************
--7. Crear un trigger en la tabla empleado que registre los datos en la tabla 
--   historico_puesto, cuando se realiza un cambio de puesto del empleado, siga 
--   la metodología según se estudió en clase. 10 puntos.
CREATE OR REPLACE TRIGGER TR_INSERT_EMPLEADO
AFTER INSERT
ON EMPLEADO
FOR EACH ROW
BEGIN
    INSERT INTO HISTORICO_PUESTO(ID_HISTORICO,CODIGO_EMPLEADO,PUESTO_ANTERIOR,PUESTO_ACTUAL,FECHA_CAMBIOS)VALUES
    (SECUENCIA.NEXTVAL,:NEW.CODIGO,:OLD.CODIGO_PUESTO,:NEW.CODIGO_PUESTO,SYSDATE);
END TR_IUD_EMPLEADO;
--******************************************************************************
--8. Crear una función que devuelva el promedio de salario de los empleados 
--   asignados a un departamento, la función debe recibir un el Código de 
--   departamento como parámetro de entrada 10 puntos.
CREATE OR REPLACE FUNCTION F_OBTIENE_PROMEDIO (P_CODIGO IN EMPLEADO.CODIGO_DEPARTAMENTO%TYPE)
RETURN FLOAT IS
PROMEDIO FLOAT;
BEGIN
    BEGIN
    SELECT ROUND(AVG(P.SALARIO),2) INTO PROMEDIO 
    FROM EMPLEADO E
    INNER JOIN DEPARTAMENTO D 
    ON E.CODIGO_DEPARTAMENTO=D.CODIGO 
    INNER JOIN PUESTO P 
    ON E.CODIGO_PUESTO=P.CODIGO
    WHERE E.CODIGO=7;
    EXCEPTION 
    WHEN NO_DATA_FOUND THEN 
        RAISE_APPLICATION_ERROR(-20001,'NO SE ENCONTRARON DATOS DEL DEPARTAMENTO'); 
    WHEN TOO_MANY_ROWS THEN     
        RAISE_APPLICATION_ERROR(-20003,'REGISTROS DUPLICADOS'); 
    WHEN OTHERS THEN     
        RAISE_APPLICATION_ERROR(-20002,'SUGIO UN ERROR NO ESPERADO');     
    END; 
RETURN PROMEDIO; 
END;
--******************************************************************************
--9. Crear la tabla. 1 puntos
CREATE TABLE PLANILLA
(IDPLANILLA INT PRIMARY KEY,
FECHA DATE,
CODIGO_EMPLEADO INT,
MONTO_SALARIO_BRUTO FLOAT,
MONTO_IVA FLOAT,
MONTO_CCSS FLOAT,
MONTO_SALARO_NETO FLOAT,
CODIGO_DEPARTAMENTO INT,
CONSTRAINT FK_CODIGO_EMPLEADO_P FOREIGN KEY(CODIGO_EMPLEADO) REFERENCES EMPLEADO(CODIGO),
CONSTRAINT FK_CODIGO_DEPARTAMENTO_P FOREIGN KEY (CODIGO_DEPARTAMENTO) REFERENCES DEPARTAMENTO(CODIGO)
);
--******************************************************************************
--10. Crear un procedimiento que genere la planilla de los empleados, para tal 
--  efecto debe crear una consulta que genere los datos necesarios para insertar
--  el resultado en la tabla planilla, dentro del procedimiento debe calcular el
--  monto del IVA que corresponde al 0.13% y el monto del CCSS que corresponde 
--  al 10% del salario del empleado, además calcule el salario neto que 
--  corresponde al salario bruto menos el monto del iva y de la CCSS, el 
--  procedimiento debe recibir por parámetro el Código del departamento del 
--  empleado. 20 puntos.
CREATE OR REPLACE PROCEDURE PA_PLANILLA_EMPLEADO IS 
BEGIN  
DECLARE CURSOR EMPLEADO IS
    SELECT E.CODIGO AS CODIGO_EMPLEADO,
    P.SALARIO AS V_MONTO_SALARIO_BRUTO,
    (P.SALARIO*0.13) AS V_MONTO_IVA,
    (P.SALARIO*0.10) AS V_MONTO_CCSS,
    ((P.SALARIO-(P.SALARIO*0.13))-(P.SALARIO*0.10)) V_MONTO_SALARIO_NETO,
    D.CODIGO AS CODIGO_DEPARTAMENTO
    FROM EMPLEADO E
    INNER JOIN PUESTO P
    ON E.CODIGO_PUESTO=P.CODIGO
    INNER JOIN DEPARTAMENTO D
    ON E.CODIGO_DEPARTAMENTO=D.CODIGO; 
BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE PLANILLA'; 
    FOR I IN EMPLEADO LOOP
    INSERT INTO PLANILLA(IDPLANILLA,FECHA,CODIGO_EMPLEADO,MONTO_SALARIO_BRUTO,
    MONTO_IVA,MONTO_CCSS,MONTO_SALARIO_NETO,CODIGO_DEPARTAMENTO) VALUES
    (SECUENCIA.NEXTVAL,SYSDATE,
    I.CODIGO_EMPLEADO,
    I.V_MONTO_SALARIO_BRUTO,
    I.V_MONTO_IVA,
    I.V_MONTO_CCSS,
    I.V_MONTO_SALARIO_NETO,
    I.CODIGO_DEPARTAMENTO);
    END LOOP;
    COMMIT;
END; 
END;
BEGIN 
PA_PLANILLA_EMPLEADO; 
END; 
--******************************************************************************
--11. Crear la tabla. 1 puntos.
CREATE TABLE ESTUDIO_AUMENTO
(IDSOLICITUD INT PRIMARY KEY,
FECHA DATE,
CODIGO_PUESTO INT,
PROCENTAGE FLOAT,
TIPO_MOVIMIENTO CHAR,
ESTADO CHAR,
CONSTRAINT CK_TIPO_MOVIMIENTO_EA CHECK (TIPO_MOVIMIENTO IN ('A', 'D')),
CONSTRAINT CK_ESTADO_EA CHECK (ESTADO IN ('A', 'P')),
CONSTRAINT FK_CODIGO_PUESTO_EA FOREIGN KEY (CODIGO_PUESTO) REFERENCES PUESTO(CODIGO)
);
--******************************************************************************
--12. Inserte dos registros para los puestos 1,6 en estado P. 1 puntos.
INSERT INTO ESTUDIO_AUMENTO VALUES(1,SYSDATE,1,0.30,'A','A');
INSERT INTO ESTUDIO_AUMENTO VALUES(2,SYSDATE,1,0.01,'A','P');
INSERT INTO ESTUDIO_AUMENTO VALUES(3,SYSDATE,1,0.10,'D','A');
INSERT INTO ESTUDIO_AUMENTO VALUES(4,SYSDATE,1,0.25,'D','P');
INSERT INTO ESTUDIO_AUMENTO VALUES(5,SYSDATE,1,0.70,'A','A');
INSERT INTO ESTUDIO_AUMENTO VALUES(6,SYSDATE,1,0.21,'A','P');
INSERT INTO ESTUDIO_AUMENTO VALUES(7,SYSDATE,1,0.16,'D','A');
INSERT INTO ESTUDIO_AUMENTO VALUES(8,SYSDATE,1,0.77,'D','P');
--******************************************************************************
--13. Crear un triguer para la tabla estudio_aumento , de manera que cuando el 
--    estado de alguno de los registros insertados en el punto 12, cambia a 'A' 
--    se modifica el salario del puesto de la siguiente manera: 
--    (salario actual (+/-) el porcentaje indicado de acuerdo al atributo tipo 
--    movimiento en la tabla de estudio_aumento). 20 puntos
--(NO COMPLETADA)
CREATE OR REPLACE TRIGGER TR_UPDATE_ESTUDIO_AUMENTO
AFTER UPDATE
ON ESTUDIO_AUMENTO
FOR EACH ROW
BEGIN
    IF :NEW.ESTADO='A'
    UPDATE PUESTO SET SALARIO=(SELECT (CASE WHEN EA.TIPO_MOVIMIENTO ='A' THEN 
                               (P.SALARIO+(P.SALARIO*EA.PROCENTAGE))
                               WHEN EA.TIPO_MOVIMIENTO ='D' THEN
                               (P.SALARIO-(P.SALARIO*EA.PROCENTAGE)) END) SALARIO
                               FROM PUESTO P
                               INNER JOIN ESTUDIO_AUMENTO EA
                               ON P.CODIGO=EA.CODIGO_PUESTO
                               WHERE P.CODIGO=:NEW.CODIGO) 
    WHERE CODIGO=:NEW.CODIGO;
    END IF;
END TR_UPDATE_ESTUDIO_AUMENTO;
--******************************************************************************
--14. Crear el Rol un el nombre INGRESADOR y asignarle los privilegios según la 
--    siguiente tabla, adjunte el script necesario. 4 puntos
CREATE ROLE INGRESADOR
IDENTIFIED BY "ADMIN12345";

GRANT SELECT ON PUESTO TO INGRESADOR;
GRANT SELECT ON DEPARTAMENTO TO INGRESADOR;
GRANT SELECT ON EMPLEADO TO INGRESADOR;

GRANT UPDATE ON PUESTO TO INGRESADOR;
GRANT UPDATE ON EMPLEADO TO INGRESADOR;

GRANT INSERT ON EMPLEADO TO INGRESADOR;
--******************************************************************************
--15. Crear la tabla 1 puntos.
CREATE TABLE INTEGRANTES
(CEDULA INT,
NOMBRE VARCHAR(100)
);
--******************************************************************************
--16. Crear los sinónimos públicos de todas las tablas, adjunte el script. 
--    4 puntos
CREATE OR REPLACE PUBLIC SYNONYM RHDEPARTAMENTO FOR RHUMANO.DEPARTAMENTO;
CREATE OR REPLACE PUBLIC SYNONYM RHPUESTO FOR RHUMANO.PUESTO;
CREATE OR REPLACE PUBLIC SYNONYM RHEMPLEADO FOR RHUMANO.EMPLEADO;
CREATE OR REPLACE PUBLIC SYNONYM RHHISTORICO_PUESTO FOR RHUMANO.HISTORICO_PUESTO;
CREATE OR REPLACE PUBLIC SYNONYM RHPLANILLA FOR RHUMANO.PLANILLA;
CREATE OR REPLACE PUBLIC SYNONYM RHESTUDIO_AUMENTO FOR RHUMANO.ESTUDIO_AUMENTO;
CREATE OR REPLACE PUBLIC SYNONYM RHINTEGRANTES FOR RHUMANO.INTEGRANTES;
SELECT * FROM DBA_OBJECTS WHERE OWNER='RHUMANO' AND OBJECT_TYPE='TABLE'
--******************************************************************************
--17. En la tabla anterior registren únicamente las cedulas y nombres de los dos 
--    integrantes del grupo, recuerde aplicar commit después de
--    registrar los datos de los nombres de los grupos. 0 puntos.
INSERT INTO INTEGRANTES VALUES(208160392,'julian enrique luque lopez')
--******************************************************************************
